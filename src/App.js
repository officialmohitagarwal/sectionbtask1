import * as React from 'react';
import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import TreeItem from '@mui/lab/TreeItem';
import data from './data.json';
import './App.css';

export default function FileSystemNavigator() {
  const getData = (key, index) => {
    const elements = data[key];
    if (Array.isArray(elements)) {
      return elements.map((el, i) => (
        <TreeItem nodeId={`${index}${i}`} label={el} />
      ));
    } else {
      return Object.keys(elements).map((el, j) => (
        <TreeItem nodeId={`${index}${j}`} label={el}>
          {data[key][el].map((ele, k) => (
            <TreeItem nodeId={`${index}${j}${k}`} label={ele} />
          ))}
        </TreeItem>
      ));
    }
  };

  return (
    <div className='container'>
      <TreeView
        aria-label='file system navigator'
        defaultCollapseIcon={<ExpandMoreIcon />}
        defaultExpandIcon={<ChevronRightIcon />}
        sx={{ height: 240, flexGrow: 1, maxWidth: 400, overflowY: 'auto' }}
      >
        {Object.keys(data).map((ele, i) => (
          <TreeItem nodeId={(i + 1).toString()} key={i} label={ele}>
            {getData(ele, i)}
          </TreeItem>
        ))}
      </TreeView>
    </div>
  );
}
